# Content

[Dataset Generation](./creation/README.md)

[Runtime data collection](#runtime-data-collection)

# Runtime data collection

The commands shown below are examples of how to extract the running time of
each query based on the system tools to do so and with the help of bash.

- **MongoDB:**

	```
	explain=`mongo --quiet file.js`
	runtime=`awk -F'[:,]' '/executionTimeMillis"/ {print $2}' <<< $explain"`
	```
- **Couchbase:**

	```
	res=`cbq -q -e $CBSERVER -c $CBUSER:$CBPASS -f file.sql 2>&1`
    info=`grep -o -P '(?<="executionTime": ").*(?=",)' <<< $res`
	parsed_info=`cbq -q -e $CBSERVER -c $CBUSER:$CBPASS -s "select str_to_duration(\"$info\");"`
	time_nanosec=`grep -o -P '(?<=\[ \{ "\$1":).*(?= \} \])' <<< $parsed_info`
	# Convert from nanoseconds to millis
	runtime=`awk -F'[:,]' "{print $time_nanosec/1000000}"`	
	```
