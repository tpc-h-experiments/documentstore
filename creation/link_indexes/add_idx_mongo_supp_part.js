conn = new Mongo();
db = conn.getDB("tpch_1");
db.supplier.createIndex( 
    {"s_parts": 1},
    {name: "idx_s_parts"}
);
db.supplier.createIndex( 
    {"s_nation.n_nationkey": 1},
    {name: "idx_n_nationkey"}
);
