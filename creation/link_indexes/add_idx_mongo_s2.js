conn = new Mongo();
db = conn.getDB("tpch_1");
db.orders.createIndex( 
    {"o_custkey": 1},
    {name: "idx_o_custkey"}
);
db.customer.createIndex( 
    {"c_nationkey": 1},
    {name: "idx_c_nationkey"}
);
db.lineitem.createIndex( 
    {"_id.l_orderkey": 1},
    {name: "idx_l_orderkey"}
);
db.lineitem.createIndex( 
    {"l_suppkey": 1},
    {name: "idx_o_custkey"}
);
db.lineitem.createIndex( 
    {"l_partkey": 1},
    {name: "idx_o_custkey"}
);
