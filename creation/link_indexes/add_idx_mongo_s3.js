conn = new Mongo();
db = conn.getDB("tpch_1");
db.orders.createIndex( 
    {"o_custkey": 1},
    {name: "idx_o_custkey"}
);
db.customer.createIndex( 
    {"c_nationkey": 1},
    {name: "idx_c_nationkey"}
);
