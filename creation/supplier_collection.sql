/*
        Export TPC-H's supplier, nation and region tables in MongoDB's
        extended JSON format
*/
\c tpch
-- Remove headers
\t on
-- Remove spacing between the results
\pset format unaligned
-- Convert relational data to JSON
set search_path to scale1;
select row_to_json(r) from
       (select 
		s_suppkey as _id,
		s_name::text,
		s_address::text,
		s_phone::text,
		s_acctbal,
		s_comment::text,
		json_build_object(
			'n_nationkey', n_nationkey,
			'n_name', n_name::text,
			'n_comment', n_comment::text,
			'n_region', json_build_object(
				    'r_regionkey', r_regionkey,
				    'r_name', r_name::text,
				    'r_comment', r_comment::text
			)
		) as s_nation,
	       json_agg(ps_partkey) as s_parts
        from 
	     supplier s, partsupp ps, nation n, region r
	where
	     s_suppkey = ps_suppkey
	     and s_nationkey = n_nationkey
	     and n_regionkey = r_regionkey
        group by 
	      _id,
	      s_name,
	      s_address,
	      s_phone,
	      s_acctbal,
	      s_comment,
	      n_nationkey,
	      r_regionkey
       ) r \g supplier_collection_mongo.json
