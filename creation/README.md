# Dataset Generation

1. Execute ``./dbgen -s <scale factor>`` inside ``/path/to/TPC\_H/dbgen/``.
2. Remove the extra ``|`` at the end of each line from each ``.tbl`` file produced 
   in the previous step. Example (bash):
   
   ```
   for i in`ls *.tbl`; do sed -i's/|$//'$i; done
   ```

3. Import the data into PostgreSQL using the ``create_rdb.sql`` script.

	- **IMPORTANT:** The script removes the database `tpch` if it already exists and it must be modified to match
		the DBGEN path in one's filesystem.

4. Sort `order.tbl` by the ``o_custkey`` attribute. Example (bash):

	```
    # sort stores temporary files in /tmp by default. If the
    # data is considerably big, one may need to use the -T option.
    sort -kn 2 -t '|' orders.tbl > orders_sorted.csv
	```
5. Sort `lineitem.tbl` by the `l_orderkey` attribute using the `orders_sorted.csv` produced from the
   previous step. Example (bash):

	```
    awk -F'|' 'NR==FNR{o[$1]=FNR; next} {print o[$1] ``|'' $0}' \
    <(awk -F'|' '{print $1}' orders_sorted.csv | uniq) \
    lineitem.tbl | sort -t '|' -nk1 | cut -d'|' -f2- > \
    lineitem_sorted.csv
	```
5. Compile the program under the ``crjoin/`` folder by running `make`.
6. Manually create the desired schemas using the program from the previous step and import
   the produced json file/s to MongoDB or Couchbase. Use the `-h` flag to check the available
   options.
