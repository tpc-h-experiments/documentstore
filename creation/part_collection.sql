/*
        Export TPC-H's part table in MongoDB's
        extended JSON format
*/
\c tpch
-- Remove headers
\t on
-- Remove spacing between the results
\pset format unaligned
-- Convert relational data to JSON
set search_path to scale1;
select row_to_json(r) from
       (select
       	       p_partkey as _id,
	       p_name::text,
	       p_mfgr::text,
	       p_brand::text,
	       p_type::text,
	       p_size,
	       p_container::text,
	       p_retailprice,
	       p_comment::text,
	       -- suppliers must have at least one part and a part at least one supplier
	       json_agg(json_build_object(
		'ps_suppkey', ps_suppkey,
		'ps_availqty', ps_availqty,
		'ps_supplycost', ps_supplycost,
		'ps_comment', ps_comment::text)) as p_suppliers
        from
	     part p, partsupp ps
	where
	     p_partkey = ps_partkey
        group by
	        _id,
		p_name,
		p_mfgr,
		p_brand,
		p_type,
		p_size,
		p_container,
		p_retailprice,
		p_comment
       ) r \g part_collection_mongo.json
