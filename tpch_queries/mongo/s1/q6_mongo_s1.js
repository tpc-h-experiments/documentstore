conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.scale1.explain("allPlansExecution").aggregate([
  {$unwind: "$c_orders" },
  {$project: {
    _id: "$c_orders.o_orderkey",
    lineitems: {
      $filter: {
	input: "$c_orders.o_lineitems",
	as: "l",
	cond: {$and:[
	  {$gte:["$$l.l_shipdate",  new Date(ISODate("1992-04-30T00:00:00Z").getTime())]},
	  {$lt:["$$l.l_shipdate",  new Date(ISODate("1993-04-30T00:00:00Z").getTime())]},
	  {$gte:["$$l.l_discount", {$subtract:[0.08, 0.01]}]},
	  {$lte:["$$l.l_discount", {$add:[0.08, 0.01]}]},
	  {$lt:["$$l.l_quantity", 24]}
	]}
    }}
  }},
  {$unwind: "$lineitems"},
  {$group:{
    _id: null,
    revenue: {$sum: {$multiply: ["$lineitems.l_extendedprice", "$lineitems.l_discount"]}}
  }}
], {allowDiskUse:true}));
