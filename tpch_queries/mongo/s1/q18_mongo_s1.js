conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.scale1.explain("allPlansExecution").aggregate([
  {$unwind: "$c_orders" },
  {$addFields:{
    sum_qty: {
      $sum: {
	$map: {
	  input: "$c_orders.o_lineitems",
	  as: "l",
	  in: "$$l.l_quantity"
    }}}
  }},
  {$match: {
    sum_qty: {$gt: 313}
  }},
  {$project: {
    _id: 1,
    c_name: 1,
    o_orderkey: "$c_orders.o_orderkey",
    o_orderdate: "$c_orders.o_orderdate",
    o_totalprice: "$c_orders.o_totalprice",
    sum_qty: 1
  }},
  {$sort:{o_totalprice: -1, o_orderdate: 1}},
  {$limit: 100}
], {allowDiskUse:true}));
