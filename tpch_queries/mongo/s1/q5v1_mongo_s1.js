conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.scale1.explain("allPlansExecution").aggregate([
  {$lookup: {
    from: "supplier",
    localField: "c_nationkey",
    foreignField: "s_nation.n_nationkey",
    as: "suppliers"
  }},
  {$project:{
    suppliers: {$filter: {
      input:"$suppliers",
      as: "s",
      cond: {$eq:["$$s.s_nation.n_region.r_name", "MIDDLE EAST"]}
    }},
    orders: {$filter: {
      input: "$c_orders",
      as: "o",
      cond: {$and: [
	{$gte:["$$o.o_orderdate", ISODate("1992-01-01T00:00:00Z")]},
	{$lt:["$$o.o_orderdate", ISODate("1993-01-01T00:00:00Z")]}
      ]}
    }}
  }},
  {$unwind: "$suppliers"},
  {$project:{
    s_suppkey: "$suppliers._id",
    n_name: "$suppliers.s_nation.n_name",
    orders: 1
  }},
  {$unwind:"$orders"},
  {$project:{
    n_name: 1,
    partial_revenue: {$sum:{
      $map:{
	input: {
	  $filter:{
	    input: "$orders.o_lineitems",
	    as: "l",
	    cond: {$eq:["$$l.l_suppkey", "$s_suppkey"]}
	  }
	},
	as: "l2",
	in: {$multiply:[
	  "$$l2.l_extendedprice",
	  {$subtract:[1, "$$l2.l_discount"]}
	]}
    }}}
  }},
  {$match:{partial_revenue:{$gt:0}}},
  {$group: {
    _id: "$n_name",
    revenue: {$sum: "$partial_revenue"}
  }},
  { $sort: {"revenue": -1} }
], {allowDiskUse:true}));
