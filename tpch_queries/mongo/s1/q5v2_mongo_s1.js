conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.supplier.explain("allPlansExecution").aggregate([
  {$match:{"s_nation.n_region.r_name":{$eq:'MIDDLE EAST'}}},
  {$project:{
    s_suppkey: "$_id",
    n_name: "$s_nation.n_name",
    n_nationkey: "$s_nation.n_nationkey"
  }},
  {$lookup: {
    from: "scale1",
    localField: "n_nationkey",
    foreignField: "c_nationkey",
    as: "customer"
  }},
  {$unwind: "$customer"},
  {$project: {
    s_suppkey: 1,
    n_name: 1,
    orders: {$filter:{
      input: "$customer.c_orders",
      as: "o",
      cond: {$and: [
	{$gte:["$$o.o_orderdate", ISODate("1992-01-01T00:00:00Z")]},
	{$lt:["$$o.o_orderdate", ISODate("1993-01-01T00:00:00Z")]}
      ]}
    }}
  }},
  {$unwind:"$orders"},
  {$project:{
    n_name: 1,
    partial_revenue: {$sum:{
      $map:{
	input: {
	  $filter:{
	    input: "$orders.o_lineitems",
	    as: "l",
	    cond: {$eq:["$$l.l_suppkey", "$s_suppkey"]}
	  }
	},
	as: "l2",
	in: {$multiply:[
	  "$$l2.l_extendedprice",
	  {$subtract:[1, "$$l2.l_discount"]}
	]}
    }}}
  }},
  {$match:{partial_revenue:{$gt:0}}},
  {$group: {
    _id: "$n_name",
    revenue: {$sum: "$partial_revenue"}
  }},
  { $sort: {"revenue": -1} }
], {allowDiskUse:true}));
