conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.part.explain("allPlansExecution").aggregate([
  {$project:{
    _id: 1,
    p_mfgr: 1,
    p_size: 1,
    p_type: 1,
    p_suppliers: 1,
    supplier_ids: {$map:{input:"$p_suppliers", as:"ps", in: "$$ps.ps_suppkey"}}
  }},
  {$lookup: {
    from: "supplier",
    localField: "supplier_ids",
    foreignField: "_id",
    as: "supplier_info"
  }},
  {$addFields:{
    supplier_info:{
      $filter: {
	input: "$supplier_info",
	as: "s",
	cond: {$eq:["$$s.s_nation.n_region.r_name", "MIDDLE EAST"]}
      }
    }
  }},
  {$addFields:{
    p_suppliers:{
      $filter: {
	input: "$p_suppliers",
	as: "ps",
	cond: {$in:["$$ps.ps_suppkey", {$map:{
	  input:"$supplier_info",
	  as:"s",
	  in:"$$s._id"
	}}]}
      }
    }
  }},
  {$match: {
    p_size: 38,
    p_type: /.*TIN/
  }},
  {$project:{
    _id: 1,
    p_mfgr: 1,
    supplier_info: 1,
    p_suppliers:1,
    min_supplycost: {$min:{$map:{input:"$p_suppliers", as:"ps", in: "$$ps.ps_supplycost"}}}
  }},
  {$unwind: "$supplier_info"},
  {$unwind: "$p_suppliers"},
  {$match: { $and: [
    {$expr:{$eq:["$p_suppliers.ps_suppkey","$supplier_info._id"]}},
    {$expr:{$eq:["$p_suppliers.ps_supplycost","$min_supplycost"]}},
  ]}},
  {$project:{
    _id: 1,
    s_acctbal: "$supplier_info.s_acctbal",
    s_name: "$supplier_info.s_name",
    n_name: "$supplier_info.s_nation.n_name",
    p_mfgr: 1,
    s_address: "$supplier_info.s_address",
    s_phone: "$supplier_info.s_phone",
    s_comment: "$supplier_info.s_comment",
    ps_supplycost: "$p_suppliers.ps_supplycost",
  }},
  {$sort:{s_acctbal:-1, n_name:1, s_name:1, _id:1}},
  {$limit: 100}
], {allowDiskUse:true}));
