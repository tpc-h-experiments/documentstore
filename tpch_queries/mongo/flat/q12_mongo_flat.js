conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.flat.explain("allPlansExecution").aggregate([
    {$match: 
     {$and: [
	 {l_shipmode:{$in:["RAIL", "REG AIR"]}},
	 {$expr:{$lt: ["$l_commitdate", "$l_receiptdate"]}},
	 {$expr:{$lt: ["$l_shipdate", "$l_commitdate"]}},
	 {l_receiptdate: {$gte:ISODate("1992-01-01T00:00:00Z")}},
	 {l_receiptdate: {$lt:ISODate("1993-01-01T00:00:00Z")}}
     ]}
    },
    { $group: {
	_id: "$l_shipmode",
	high_line_count: {$sum: {
	    $switch: {
		branches: [
		    { 
			case: {$or:[{$eq:["$o_orderpriority", "1-URGENT"]}, 
				    {$eq:["$o_orderpriority", "2-HIGH"]}]},
			then: 1
		    }
		],
		default: 0
	    }
	}},
	low_line_count: {$sum: {
	    $switch: {
		branches: [
		    {
			case: {$and:[{$ne:["$o_orderpriority", "1-URGENT"]}, 
				     {$ne:["$o_orderpriority", "2-HIGH"]}]},
			then: 1
		    }
		],
		default: 0
	    }
	}}
    }},
    { $sort: { "_id": 1 } }
]));
