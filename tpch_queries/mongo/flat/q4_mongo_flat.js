conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.flat.explain("allPlansExecution").aggregate([
    {$match: 
     {$and: 
      [
	  {o_orderdate: {$gte:ISODate("1992-01-01T00:00:00Z")}}, 
	  {o_orderdate: {$lt:ISODate("1992-04-01T00:00:00Z")}},
	  {$expr:{$lt:["$l_commitdate","$l_receiptdate"]}}
      ]
     }
    },
    { $group: {
	_id: {orderkey:"$o_orderkey", orderpriority:"$o_orderpriority"}
    }},
    { $group: {
	_id: "$_id.orderpriority",
	order_count: {$sum: 1}
    }},
    { $sort: {"_id": 1}}
]));
