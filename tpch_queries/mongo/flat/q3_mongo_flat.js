conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.flat.explain("allPlansExecution").aggregate([
    { $match: 
      {$and:
       [
	   {c_mktsegment: "AUTOMOBILE"}, 
	   {o_orderdate: {$lt: ISODate("1992-01-02T00:00:00Z")}},
	   {l_shipdate: {$gt: ISODate("1992-01-02T00:00:00Z")}}
       ]
      }
    },
    { $group: { 
    	_id: {l_orderkey: "$o_orderkey", o_orderdate: "$o_orderdate", o_shippriority: "$o_shippriority"},
    	revenue: {$sum: {$multiply: ["$l_extendedprice", {$subtract: [1, "$l_discount"]}]}}
    }},
    { $sort: { "revenue": -1, "_id.o_orderdate": 1 }},
    { $limit : 10 }
]));
