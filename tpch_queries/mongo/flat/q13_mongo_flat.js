conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.flat.explain("allPlansExecution").aggregate([
    { $match: {
    	o_comment: {$not: /.*express.*packages.*/}
    }},
    {$group: {
	_id: {o_orderkey: "$o_orderkey", c_custkey: "$c_custkey"}
    }},
    { $group: {
    	_id: "$_id.c_custkey",
    	c_count: { $sum: {
    	    $cond: [ {$eq:["$_id.o_orderkey", null]}, 0, 1 ]
    	}}
    }},
    { $group: {
    	_id: "$c_count",
    	custdist: {$sum: 1}
    }},
    {$sort: {"custdist": -1, "_id": -1}}
], { allowDiskUse: true }
));
