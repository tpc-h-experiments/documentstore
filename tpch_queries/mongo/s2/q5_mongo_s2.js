conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.orders.explain("allPlansExecution").aggregate([
  {$match:{$expr:{
    $and:[
      {$gte:["$o_orderdate", ISODate("1992-01-01T00:00:00Z")]},
      {$lt:["$o_orderdate", ISODate("1993-01-01T00:00:00Z")]}
    ]
  }}},
  {$lookup: {
    from: "lineitem",
    localField: "_id",
    foreignField: "_id.l_orderkey",
    as: "lineitems"
  }},
  {$lookup: {
    from: "customer",
    localField: "o_custkey",
    foreignField: "_id",
    as: "customer"
  }},
  // Only one customer
  {$unwind: "$customer"},
  {$lookup: {
    from: "supplier",
    localField: "customer.c_nationkey",
    foreignField: "s_nation.n_nationkey",
    as: "suppliers"
  }},
  {$project:{
    suppliers: {$filter: {
      input:"$suppliers",
      as: "s",
      cond: {$eq:["$$s.s_nation.n_region.r_name", "MIDDLE EAST"]}
    }},
    lineitems: 1
  }},
  {$unwind: "$suppliers"},
  {$project:{
    n_name: "$suppliers.s_nation.n_name",
    partial_revenue: {$sum:{
      $map:{
	input: {
	  $filter:{
	    input: "$lineitems",
	    as: "l",
	    cond: {$eq:["$$l.l_suppkey", "$suppliers._id"]}
	  }
	},
	as: "l2",
	in: {$multiply:[
	  "$$l2.l_extendedprice",
	  {$subtract:[1, "$$l2.l_discount"]}
	]}
    }}}
  }},
  {$match:{partial_revenue:{$gt:0}}},
  {$group: {
    _id: "$n_name",
    revenue: {$sum: "$partial_revenue"}
  }},
  { $sort: {"revenue": -1} }
], {allowDiskUse:true}));
