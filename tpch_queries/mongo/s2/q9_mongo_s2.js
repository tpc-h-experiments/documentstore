conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.part.explain("allPlansExecution").aggregate([
  {$match: {p_name:/.*yellow.*/}},
  {$lookup:{
    from: "lineitem",
    localField: "_id",
    foreignField: "l_partkey",
    as: "lineitem"
  }},
  {$unwind: "$p_suppliers"},
  {$project:{
    p_partkey: "$_id",
    ps_supplycost: "$p_suppliers.ps_supplycost",
    ps_suppkey: "$p_suppliers.ps_suppkey",
    lineitem: 1
  }},
  {$lookup: {
    from: "supplier",
    localField: "ps_suppkey",
    foreignField: "_id",
    as: "supplier"
  }},
  {$unwind: "$supplier"},
  {$match: {$expr:{$in:["$p_partkey", "$supplier.s_parts"]}}},
  {$project:{
    p_partkey: 1,
    ps_supplycost: 1,
    ps_suppkey: 1,
    n_nation: "$supplier.s_nation.n_name",
    lineitem: { $filter: {
      input:"$lineitem",
      as:"l",
      cond: {$eq:["$$l.l_suppkey", "$ps_suppkey"]}
    }}
  }},
  {$unwind:"$lineitem"},
  {$lookup: {
    from: "orders",
    localField: "lineitem._id.l_orderkey",
    foreignField: "_id",
    as: "order"
  }},
  {$unwind: "$order"},
  {$project: {
    nation: "$n_nation",
    o_year: { $year: "$order.o_orderdate"},
    amount: {$subtract: [
      {$multiply:["$lineitem.l_extendedprice", {$subtract: [1, "$lineitem.l_discount"]} ]}, 
      {$multiply:["$ps_supplycost", "$lineitem.l_quantity"]}
    ]}
  }},
  {$group: {
    _id: { nation:"$nation", o_year:"$o_year"},
    sum_profit: {$sum: "$amount"}
  }},
  { $sort: { "_id.nation": 1, "_id.o_year": -1} }
], {allowDiskUse:true}));
