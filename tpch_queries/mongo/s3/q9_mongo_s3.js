conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.orderlineitems.explain("allPlansExecution").aggregate([
  {$project:{
    o_orderkey: "$_id",
    o_orderdate: 1,
    o_lineitems: 1
  }},
  {$unwind:"$o_lineitems"},
  {$project:{
    o_orderkey: 1,
    o_orderdate: 1,
    l_discount: "$o_lineitems.l_discount",
    l_quantity: "$o_lineitems.l_quantity",
    l_extendedprice: "$o_lineitems.l_extendedprice",
    l_partkey: "$o_lineitems.l_partkey",
    l_suppkey: "$o_lineitems.l_suppkey"
  }},
  {$lookup:{
    from: "part",
    localField: "l_partkey",
    foreignField: "_id",
    as: "part"
  }},
  {$unwind: "$part"},
  {$match: { "part.p_name": /.*yellow.*/} },
  {$project: {
    o_orderkey: 1,
    o_orderdate: 1,
    l_discount: 1,
    l_quantity: 1,
    l_extendedprice: 1,
    l_partkey: 1,
    l_suppkey: 1,
    p_suppliers: {
      $filter: {
	input: "$part.p_suppliers",
	as: "ps",
	cond: {$eq:["$$ps.ps_suppkey", "$l_suppkey"]}
      }
    }
  }},
  {$unwind: "$p_suppliers"},
  {$lookup:{
    from: "supplier",
    localField: "l_suppkey",
    foreignField: "_id",
    as: "supplier"
  }},
  {$unwind: "$supplier"},
  {$project: {
    nation: "$supplier.s_nation.n_name",
    o_year: { $year: "$o_orderdate"},
    amount: {$subtract: [
      {$multiply:["$l_extendedprice", {$subtract: [1, "$l_discount"]} ]}, 
      {$multiply:["$p_suppliers.ps_supplycost", "$l_quantity"]}
    ]}
  }},
  {$group: {
    _id: { nation:"$nation", o_year:"$o_year"},
    sum_profit: {$sum: "$amount"}
  }},
  { $sort: { "_id.nation": 1, "_id.o_year": -1} }
], {allowDiskUse:true}));
