conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.orderlineitems.explain("allPlansExecution").aggregate([
  {$addFields:{
    sum_qty: {
      $sum: {
	$map: {
	  input: "$o_lineitems",
	  as: "l",
	  in: "$$l.l_quantity"
    }}}
  }},
  {$match: {
    sum_qty: {$gt: 313}
  }},
  {$lookup:{
    from: "customer",
    localField: "o_custkey",
    foreignField: "_id",
    as: "customer"    
  }},
  {$unwind: "$customer"},
  {$project: {
    _id: "$o_custkey",
    c_name: "$customer.c_name",
    o_orderkey: 1,
    o_orderdate: 1,
    o_totalprice: 1,
    sum_qty: 1
  }},
  {$sort:{o_totalprice: -1, o_orderdate: 1}},
  {$limit: 100}
], {allowDiskUse:true}));
